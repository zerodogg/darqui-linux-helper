# darqui-linux-helper

URL opening helper script for DarqUI/EverQuest 2 on Linux.

`darqui-linux-helper` is a small helper script that replicates the URL helper parts of the
[DarqUI](https://eq2interface.com/downloads/info4997-DarqUIUnified.html) notify
utility on Linux. DarqUI is a custom interface for the game [EverQuest
2](https://www.everquest2.com/home). The custom UI combined with this script
allows URLs in-game to be opened up in the system browser, rather than the
(broken) in-game browser.

## Installation and usage

First install required packages. Follow the instructions matching your
distribution below.

**Debian** and derivatives (Ubuntu, Pop!_OS, Mint, Elementary, ..):
`sudo apt install perl git liblinux-inotify2-perl`

**Fedora** and derivatives (RedHat, CentOS, ..): `sudo dnf install perl git
perl-Linux-Inotify2`

**Other**: install the distribution packages for `perl`, `git` and the
`Linux::Inotify2` perl module. `Linux::Inotify2` is optional, but recommended.

Now, install the program by cloning the git repository: `git clone
https://gitlab.com/zerodogg/darqui-linux-helper.git ~/.darqui-linux-helper`  
The location doesn't matter, feel free to change it.

You may now use the helper by running:
`~/.darqui-linux-helper/darqui-linux-helper`  
It will attempt to locate your EverQuest 2 installation automatically, but this
might fail, if it does you will need to tell it where it is:
`~/.darqui-linux-helper/darqui-linux-helper /path/to/EQ2`

## Enabling the feature in DarqUI

Assuming you already have DarqUI installed, simply run `darqui-linux-helper
--enable`

## Listing training mercenaries

In addition to working as a URL helper, `darqui-linux-helper` can also list
your training mercenaries, with remaining time - as long as you have reminders
set using DarqUI.

Use `darqui-linux-helper --list-merc-training`. This can be used regardless of
the URL helper.

## License

darqui-linux-helper is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

darqui-linux-helper is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
darqui-linux-helper. If not, see http://www.gnu.org/licenses/.
